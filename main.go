package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sort"
)

func main() {
	printFiles(`./`, 0)

	return
}

func printFiles(path string, level int) {
	tbl := make(map[int]bool)
	listFiles(path, level, tbl)
}

func listFiles(path string, level int, tbl map[int]bool) {
	files, err := ioutil.ReadDir(path)
	sorting(files)

	count := 0
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		last := isLast(count, len(files)-1)
		tbl[level] = last

		if file.IsDir() {
			printString(file.Name(), level, last, tbl)
			listFiles(path+`/`+file.Name(), level+1, tbl)
		} else {
			printString(file.Name(), level, last, tbl)
		}
		count++
	}

}

func sorting(files []os.FileInfo) {
	sort.Slice(files, func(i, j int) bool {
		return files[i].Name() < files[j].Name()
	})
}

func isLast(count int, files int) bool {
	if count == files {
		return true
	}

	return false
}

func printString(str string, level int, last bool, tbl map[int]bool) {
	spaces := ""

	for i := 0; i < level; i++ {
		if tbl[i] {
			spaces += `    `
		} else {
			spaces += `│   `
		}
	}
	if last {
		spaces += `└───`
	} else {
		spaces += `├───`
	}

	fmt.Println(spaces + str)
}
