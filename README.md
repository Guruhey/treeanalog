<h2>run go main.go</h2>
<pre>
<h2>EXAMPLE</h2>
├───treeanalog
├───.git
│   ├───FETCH_HEAD
│   ├───HEAD
│   ├───config
│   ├───description
│   ├───hooks
│   │   ├───applypatch-msg.sample
│   │   ├───commit-msg.sample
│   │   ├───fsmonitor-watchman.sample
│   │   ├───post-update.sample
│   │   ├───pre-applypatch.sample
│   │   ├───pre-commit.sample
│   │   ├───pre-merge-commit.sample
│   │   ├───pre-push.sample
│   │   ├───pre-rebase.sample
│   │   ├───pre-receive.sample
│   │   ├───prepare-commit-msg.sample
│   │   └───update.sample
│   ├───index
│   ├───info
│   │   └───exclude
│   ├───logs
│   │   ├───HEAD
│   │   └───refs
│   │       ├───heads
│   │       │   └───main
│   │       └───remotes
│   │           └───origin
│   │               └───HEAD
│   ├───objects
│   │   ├───info
│   │   └───pack
│   │       ├───pack-cc54de081e1e0111358b536219e1d13af9e9ee80.idx
│   │       └───pack-cc54de081e1e0111358b536219e1d13af9e9ee80.pack
│   ├───packed-refs
│   └───refs
│       ├───heads
│       │   └───main
│       ├───remotes
│       │   └───origin
│       │       └───HEAD
│       └───tags
└───README.md
</pre>

